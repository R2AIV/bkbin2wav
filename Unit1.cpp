//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <vector>

FILE *BinFile;
FILE *WavFile;

typedef struct
{
        uint32_t chunkId;
	uint32_t chunkSize;
	uint32_t format;
	uint32_t subchunk1Id;
	uint32_t subchunk1Size;
	uint16_t audioFormat;
	uint16_t numChannels;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint16_t blockAlign;
	uint16_t bitsPerSample;
	uint32_t subchunk2Id;
	uint32_t subchunk2Size;   
} WavHeader;

typedef struct
{
        uint8_t file_name[16];
        uint16_t file_length;
	uint16_t load_addr;
} file_header;

uint8_t BinDataBuffer[1048576];
uint8_t WaveDataBuffer[13230000];

uint16_t BinSize=0;
uint16_t BinOffset=0;

// ������ ����� (���. 1)
void EmptyData(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        for(uint8_t i=0;i<100;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 200;
        };
        *WavFilePos += 100;

}

// ��������� ��������
// ������� ������������� 44100, ������ 22.6 ���
// ������� ������� 2600, ������ 384.6 ���
// ������ �� ������� 17, ���������� - 8.5 (8)
void SingleInterval(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        for(uint8_t i=0;i<8;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 200;
        };
        *WavFilePos += 8;

        for(uint8_t i=0;i<8;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 50;
        };
        *WavFilePos += 8;
};

// ������� ��������
void DoubleInterval(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        uint32_t i;
        for(uint8_t i=0;i<16;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 200;
        };
        *WavFilePos += 16;

        for(uint8_t i=0;i<16;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 50;
        };
        *WavFilePos += 16;
}

// ������� ��������
void TripleInterval(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        uint32_t i;
        for(uint8_t i=0;i<24;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 200;
        };
        *WavFilePos += 24;

        for(uint8_t i=0;i<24;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 50;
        };
        *WavFilePos += 24;
};

// ��������� ��������
void SixthInterval(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        uint32_t i;
        for(uint8_t i=0;i<96;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 200;
        };
        *WavFilePos += 96;

        for(uint8_t i=0;i<96;i++)
        {
                WaveFileBuffer[(*WavFilePos+i)] = 50;
        };
        *WavFilePos += 96;
}

// ������������� ������������������ 
void calib_seq(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        for(uint16_t i=0;i<=4096;i++)
                SingleInterval(WaveFileBuffer, WavFilePos);
}

// ������������������������
void sync_seq(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        TripleInterval(WaveFileBuffer, WavFilePos);
        DoubleInterval(WaveFileBuffer, WavFilePos);
        SingleInterval(WaveFileBuffer, WavFilePos);
}

// �������� ������������������������
void end_sync(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        uint16_t i=0;
        for(i=0;i<=255;i++) SingleInterval(WaveFileBuffer, WavFilePos);
}

// ������ ������
void begin_mark(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        for(uint8_t i=0;i<8;i++) SingleInterval(WaveFileBuffer,WavFilePos);
}

// �������
void one(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        DoubleInterval(WaveFileBuffer, WavFilePos);
        SingleInterval(WaveFileBuffer, WavFilePos);
}

// ����
void zero(uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        SingleInterval(WaveFileBuffer, WavFilePos);
        SingleInterval(WaveFileBuffer, WavFilePos);
}

// �������� ����� ������
void tx_byte(uint8_t Data, uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        uint8_t i=0;
	for(i=0;i<8;i++)
	{
		if(Data & (1 << i)) one(WaveFileBuffer, WavFilePos);
		else zero(WaveFileBuffer, WavFilePos);
	}
}

// �������� ���������
void tx_header(file_header * head, uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
	uint8_t i=0;
	uint8_t *tmp=0;
	for(i=0;i<sizeof(head);i++)
	{
		tmp = (uint8_t *)&head + i;
		tx_byte(*tmp, WaveFileBuffer, WavFilePos);
	}
}

// �������� ������
void tx_data(uint8_t *data, uint16_t size,  uint8_t *WaveFileBuffer, uint32_t *WavFilePos)
{
        uint16_t i=0;
	for(i=0;i<size;i++)
	{
		tx_byte(data[i], WaveFileBuffer, WavFilePos);
	}
}

uint16_t bk_crc(uint8_t *Buffer, uint16_t Size)
{
        uint32_t CRC = 0;

        for(uint16_t i=0;i<(Size);i++)
        {
                CRC += Buffer[i];
                CRC += (CRC & 0200000) != 0;
                CRC &= 0xFFFF;
        }
        
        return (CRC & 0xFFFF);
}

TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
        uint16_t RealRead = 0;

        uint8_t OCT_LoadAddr[10];
        uint8_t OCT_Size[10];
        uint8_t OCT_CRC[10];

        uint16_t CRC=0;

        Shape2->Brush->Color = clRed;

        if(!OpenDialog1->Execute())
        {
                MessageBox(NULL,"���������?","��������!",MB_ICONEXCLAMATION);
                return;
        };

        Edit1->Text = OpenDialog1->FileName;
        BinFile = fopen(Edit1->Text.c_str(), "rb");
        memset((void *)BinDataBuffer, 0, sizeof(BinDataBuffer));

        // ������ ������ � �������� �� �����
        fread(&BinOffset, sizeof(uint16_t), 1, BinFile);
        fread(&BinSize, sizeof(uint16_t),1,BinFile);

        // ������ ���������� � �����
        RealRead = fread(BinDataBuffer,sizeof(uint8_t),BinSize,BinFile);

        fclose(BinFile);

        CRC = bk_crc(BinDataBuffer,BinSize);

        sprintf(OCT_LoadAddr,"%o",BinOffset);
        sprintf(OCT_Size, "%o",BinSize);
        sprintf(OCT_CRC, "%o", CRC);

        Edit2->Text = String((char *)OCT_LoadAddr);
        Edit3->Text = String((char *)OCT_Size);
        Edit5->Text = String((char *)OCT_CRC);
        Shape1->Brush->Color = clGreen;
        Button1->Enabled = false;
        Button2->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
        WavHeader hdr;
        uint32_t WaveFilePos = 0;
        uint8_t Tmp=0;
        uint16_t CRC=0;
        file_header bk_hdr;
        uint8_t TapeFilename[16];

        memset((void *)WaveDataBuffer,0,sizeof(WaveDataBuffer));
        memset((void *)TapeFilename,' ',sizeof(TapeFilename));

        memcpy((void *)&hdr.chunkId, "RIFF",4);
        memcpy((void *)&hdr.format, "WAVE",4);
        memcpy((void *)&hdr.subchunk1Id, "fmt ",4);
        memcpy((void *)&hdr.subchunk2Id, "data",4);

        hdr.subchunk1Size = 16; // PCM
        hdr.numChannels = 1; // ����
        hdr.sampleRate = 44100;
        hdr.byteRate = 44100;
        hdr.blockAlign = 1;
        hdr.bitsPerSample = 8;
        hdr.audioFormat = 1;

        if(strlen(Edit6->Text.c_str()) >= 15)
        {
                MessageBox(NULL,"������� ������� ��� ����� �� �����","!",MB_ICONSTOP);
                return;
        }

        if(Edit6->Text == "")
        {
                MessageBox(NULL,"������ ��� ����� �� �����!","!",MB_ICONSTOP);
                return;
        }

        memset((void *)&bk_hdr, 0, sizeof(bk_hdr));
        memcpy((char *)TapeFilename, Edit6->Text.c_str(), strlen(Edit6->Text.c_str()));
        memcpy((char *)&bk_hdr.file_name, TapeFilename,strlen(TapeFilename));
        bk_hdr.load_addr = BinOffset;
        bk_hdr.file_length = BinSize;

        SaveDialog1->FileName = Edit6->Text + ".wav";

        SaveDialog1->Execute();
        Edit4->Text = SaveDialog1->FileName;

        CRC = bk_crc(BinDataBuffer,BinSize);

        WavFile = fopen(SaveDialog1->FileName.c_str(),"wb");
        /* ������ */
        EmptyData(WaveDataBuffer, &WaveFilePos);
        calib_seq(WaveDataBuffer, &WaveFilePos);
        sync_seq(WaveDataBuffer, &WaveFilePos);
        begin_mark(WaveDataBuffer, &WaveFilePos);
        sync_seq(WaveDataBuffer, &WaveFilePos);
        tx_data((uint8_t *)&bk_hdr.load_addr, 2, WaveDataBuffer, &WaveFilePos);
        tx_data((uint8_t *)&bk_hdr.file_length, 2, WaveDataBuffer, &WaveFilePos);
        tx_data(bk_hdr.file_name, 16, WaveDataBuffer, &WaveFilePos);
        begin_mark(WaveDataBuffer, &WaveFilePos);
        sync_seq(WaveDataBuffer, &WaveFilePos);
        tx_data(BinDataBuffer, BinSize, WaveDataBuffer, &WaveFilePos);
        tx_data((uint8_t *)&CRC, 2, WaveDataBuffer, &WaveFilePos);
        SixthInterval(WaveDataBuffer, &WaveFilePos);
        end_sync(WaveDataBuffer, &WaveFilePos);
        sync_seq(WaveDataBuffer, &WaveFilePos);
        /* ����� */
        hdr.subchunk2Size = WaveFilePos;

        fwrite((void *)&hdr, sizeof(hdr),1,WavFile);
        fwrite((void *)WaveDataBuffer, WaveFilePos,1,WavFile);

        fclose(WavFile);
        Button2->Enabled = false;
        Button1->Enabled = true;
        Shape1->Brush->Color = clRed;
        Shape2->Brush->Color = clGreen;
}
//---------------------------------------------------------------------------
