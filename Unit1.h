//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <jpeg.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabel *Label1;
        TEdit *Edit1;
        TButton *Button1;
        TLabel *Label2;
        TLabel *Label3;
        TOpenDialog *OpenDialog1;
        TEdit *Edit2;
        TEdit *Edit3;
        TShape *Shape1;
        TGroupBox *GroupBox2;
        TLabel *Label4;
        TEdit *Edit4;
        TButton *Button2;
        TSaveDialog *SaveDialog1;
        TLabel *Label5;
        TEdit *Edit5;
        TShape *Shape2;
        TImage *Image1;
        TLabel *Label6;
        TEdit *Edit6;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
